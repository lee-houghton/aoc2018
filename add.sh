#!/bin/bash -e
echo "add_executable(p$1 src/p$1.cpp \${AOC_HEADERS})" >> CMakeLists.txt
touch src/p$1.cpp input/p$1.txt
cat <<EOF >> src/p$1.cpp 
#include "common.hpp"

using namespace std;

int main() {
    cout << "Problem $1" << endl;
}
EOF
code src/p$1.cpp input/p$1.txt
cmake . && make p$1 && ./bin/p$1