#include "common.hpp"

using namespace std;

struct rect {
    int id, x, y, w, h;
};

int main() {
    ifstream input("input/p3.txt");
    input >> skipws;
    
    vector<rect> rects;
    while (!input.eof()) {
        string line;
        getline(input, line);
        if (line.length() == 0)
            break;
        std::replace(line.begin(), line.end(), '#', ' ');
        std::replace(line.begin(), line.end(), ',', ' ');
        std::replace(line.begin(), line.end(), '@', ' ');
        std::replace(line.begin(), line.end(), ':', ' ');
        std::replace(line.begin(), line.end(), 'x', ' ');
        rect r;
        stringstream(line) >> r.id >> r.x >> r.y >> r.w >> r.h;
        rects.push_back(r);
    }

    int disputes = 0;
    constexpr int size = 1000;
    vector<int> grid(size * size);
    for (rect& r: rects)
        for (int y = r.y; y < r.y + r.h; y++)
            for (int x = r.x; x < r.x + r.w; x++)
                if (++grid[y * size + x] == 2)
                    disputes++;

    cout << "There are " << disputes << " disputes." << endl;

    for (rect& r: rects)
        if (find_if(rects.begin(), rects.end(), [&r](rect& r2) {
            return !(
                r.id == r2.id
                || r.x >= r2.x + r2.w
                || r.y >= r2.y + r2.h
                || r2.x >= r.x + r.w
                || r2.y >= r.y + r.h
            );
        }) == rects.end())
            cout << "Rectangle " << r.id << " has no overlaps" << endl;
}