#include "common.hpp"

using namespace std;

auto read_input() {
    ifstream input("input/p4.txt");
    input >> skipws;

    vector<string> lines;
    while(!input.eof()) {
        string line;
        getline(input, line);
        if (line.length() == 0)
            break;
        lines.push_back(line);
    }

    sort(lines.begin(), lines.end());
    return lines;
}

auto parse_input(const vector<string>& lines) {
    map<int, int> guard_sleepiness;
    map<int, map<int, int>> guard_minutes;

    int guard = -1, start_minute;
    for (const string& line: lines) {
        if (line.length() < 20)
            continue;

        int hour, minute;
        stringstream(line.substr(15, 2)) >> minute;

        switch(line[19]) {
            case 'G': 
                stringstream(line.substr(26)) >> guard;
                break;
            case 'f':
                start_minute = minute;
                break;
            case 'w':
                guard_sleepiness[guard] += (minute - start_minute);
                for (int m = start_minute; m < minute; m++)
                    guard_minutes[guard][m]++;
                break;
        }
    }

    return tuple(move(guard_sleepiness), move(guard_minutes));
}

int main() {
    auto [guard_sleepiness, guard_minutes] = parse_input(read_input());

    int sleepiest_guard = max_element(
        guard_sleepiness.begin(),
        guard_sleepiness.end(),
        [](auto& x, auto& y) { return x.second < y.second; }
    )->first;

    int sleepiest_minute = max_element(
        guard_minutes[sleepiest_guard].begin(),
        guard_minutes[sleepiest_guard].end(),
        [](auto& x, auto& y) { return x.second < y.second; }
    )->first;

    cout << "Part 1 answer is " << (sleepiest_guard * sleepiest_minute) << endl;

    int sleepiest_predictable_minute = -1, sleepiest_predictable_minute_guard = -1, sleepiest_predictable_minute_count = 0;
    for (const auto& [g, mins]: guard_minutes) {
        for (const auto& [m, count]: mins) {
            if (count > sleepiest_predictable_minute_count) {
                sleepiest_predictable_minute_guard = g;
                sleepiest_predictable_minute = m;
                sleepiest_predictable_minute_count = count;
            }
        }
    }

    cout << "Sleepiest predictable minute is for guard " << sleepiest_predictable_minute_guard << " at 00h" << sleepiest_predictable_minute << "m with " << sleepiest_predictable_minute_count << " occurrences" << endl;
    cout << "Answer for part 2 is " << (sleepiest_predictable_minute_guard * sleepiest_predictable_minute) << endl;
}