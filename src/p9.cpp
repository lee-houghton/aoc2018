#include "common.hpp"
#include "iter.hpp"

using namespace std;

void part1(int number_of_players, long biggest_marble, bool debug = false) {
    list<long> marbles = { 0 };
    vector<long> scores(number_of_players);
    auto pos = marbles.end();
    int player = 0;

    if (debug)
        cout << "[-] M0: 0" << endl;

    for (long marble = 1; marble <= biggest_marble; marble++) {
        if (marble % 23 != 0) {
            // Move right twice and insert the new marble
            cyclic_advance(marbles, pos, 2);
            pos = marbles.insert(pos, marble);
        } else { 
            scores[player] += marble; // Keep this marble, don't insert it
            cyclic_retreat(marbles, pos, 7); // Move left 7 times
            // Remove this marble and add its value to the player's score
            scores[player] += *pos;
            pos = marbles.erase(pos);
        }

        if (debug) {
            cout << "[" << player << "] M" << marble << ": ";
            copy(marbles.begin(), marbles.end(), ostream_iterator<int>(cout, " "));
            cout << endl;
        }

        player = (player + 1) % number_of_players;
    }

    if (debug) {
        cout << "Scores:" << endl;
        copy(scores.begin(), scores.end(), ostream_iterator<int>(cout, " "));
        cout << endl;
    }

    cout << "Highest score for " << number_of_players << " players and " << biggest_marble << " marbles is: " << 
        *std::max_element(scores.begin(), scores.end()) << endl;
}

int main() {
    part1(5, 25, true);
    part1(10, 1618);
    part1(13, 7999);
    part1(17, 1104);
    part1(21, 6111);
    part1(30, 5807);
    part1(441, 71032);
    part1(441, 71032 * 100);
}
