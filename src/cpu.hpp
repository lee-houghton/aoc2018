#include <map>
#include <string>
#include <vector>

namespace time_travel_cpu {
    enum opcode {
        addr,
        addi,
        mulr,
        muli,
        bani,
        banr,
        bori,
        borr,
        seti,
        setr,
        gtri,
        gtir,
        gtrr,
        eqrr,
        eqri,
        eqir
    };

    bool is_comparison(opcode op);

    struct instruction {
        opcode op;
        int a, b, c;
        std::string op_name;
    };

    struct program {
        int ip_register;
        std::vector<instruction> instructions;
    };

    class cpu {
        int r[6] = { 0, 0, 0, 0, 0, 0 };
        bool debug = false;

    public:
        cpu(bool debug = false): debug(debug) {
        }

        void run(const program& program);

        void execute(const instruction& ixn, int ip_register);

        void show_registers();

        void set_register(int reg, int value);
    };

    program read_program(const char* path);

    // No disassemble!
    void disassemble(const program& prog, const char* output_path);
}
