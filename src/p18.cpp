#include "common.hpp"
#include "grid.hpp"

using namespace std;

auto read_lumber_collection_area_from_file(char const* path) {
    auto lines = read_lines(path);
    if (lines.empty())
        throw range_error("Input is empty");
    grid<char> lca(lines[0].length(), lines.size());
    for (int y = 0; y < lines.size(); y++)
        for (int x = 0; x < lines[y].length() && x < lca.width(); x++)
            lca.at(x, y) = lines[y][x];
    return lca;
}

// Simulate a single minute of `lca` and put the result in `lca2`.
void simulate_lumber_collection_area(grid<char>& lca, grid<char>& lca2) {
    for (int y = 0; y < lca.height(); y++) {
        for (int x = 0; x < lca.width(); x++) {
            int adj_trees = 0, adj_lumberyards = 0;
            for (int u = -1; u <= 1; u++) {
                for (int v = -1; v <= 1; v++) {
                    if (!lca.valid_index(x + u, y + v))
                        continue;
                    if (u == 0 && v == 0)
                        continue;
                    char contents = lca.at(x + u, y + v);
                    if (contents == '|')
                        adj_trees++;
                    else if (contents == '#')
                        adj_lumberyards++;
                }
            }

            char current = lca.at(x, y), next = current;
            if (current == '.' && adj_trees >= 3)
                next = '|';
            else if (current == '|' && adj_lumberyards >= 3)
                next = '#';
            else if (current == '#' && !(adj_lumberyards > 0 && adj_trees > 0))
                next = '.';
            
            lca2.at(x, y) = next;
        }
    }
}

int resource_value(grid<char> const& lca) {
    auto trees = count(lca.begin(), lca.end(), '|');
    auto lumberyards = count(lca.begin(), lca.end(), '#');
    return trees * lumberyards;
}

string calculate_checksum(grid<char> const& lca) {
    // Simple RLE encoding to reduce memory usage of the seen states
    // Since we don't know the periodicity, which might be high
    ostringstream ss;
    ss << hex;
    char last = lca.at(0, 0);
    int count = -1;
    for (char c: lca) {
        if (c == last) {
            count++;
        } else {
            ss << (count + 1) << last;
            count = 0;
        }

        last = c;
    }
    return ss.str();
}

int main() {
    auto lca = read_lumber_collection_area_from_file("input/p18.txt");
    auto lca2 = lca;
    
    constexpr int initial_status_report_minute = 10;
    constexpr int long_range_simulation_minutes = 1000000000;

    map<string, int> seen_states;

    // This is very similar to the problem with the plant pots. Since the algorithm is deterministic,
    // just wait for a state that is observed twice and you know the period of the repetition. Then
    // just wait until you're at the correct part of the cycle to line up with 1 billion minutes.
    //
    // In my case, minute 440 is the same as 412, and we know that minute 1000000000 will be the same
    // since 1e9 = 440 + (n * (440 - 412)) where n = 35714270 in this case.
    for (int minute = 1; minute <= long_range_simulation_minutes; minute++) {
        simulate_lumber_collection_area(lca, lca2);
        swap(lca, lca2);

        if (minute == initial_status_report_minute) {
            cout << "Resource value after " << minute << " minutes is " << resource_value(lca) << endl;
        }

        auto cs = calculate_checksum(lca);
        auto prev = seen_states.find(cs);
        if (prev != seen_states.end()) {
            int prev_minute = prev->second;
            int period = minute - prev_minute;
            cout << "Cycle detected: minutes " << prev_minute << " and " << minute << " are the same" << endl;
            if ((long_range_simulation_minutes - minute) % period == 0) {
                cout << "Resource value at " << long_range_simulation_minutes << " will be " << resource_value(lca) << endl;
                break;
            }
        } else {
            seen_states.insert(prev, pair(cs, minute));
        }
    }
}

