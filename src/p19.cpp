#include "common.hpp"
#include "cpu.hpp"

using namespace std;

inline int eq(int x, int y) { return x == y ? 1 : 0; }
inline int gt(int x, int y) { return x > y ? 1 : 0; }

int part_2_slow(int a) {
    int b = 0, c = 0, d = 0, e = 0, f = 0;

    L0: goto L17;
    L1: c = 1;
    L2: f = 1;
    L3: d = c * f;
    L4: d = eq(d, b);
    L5: if (d) goto L7;
    L6: goto L8;
    L7: a += c;
    L8: f++;
    L9: d = gt(f, b);
    L10: if (d) goto L12;
    L11: goto L3;
    L12: c++;
    L13: d = gt(c, b);
    L14: if (d) goto L16;
    L15: goto L2;
    L16: cout << "L16: " << a << " " << b << " " << c << " " << d << " " << e << " " << f << endl; return a; // END
    L17: b += 2;
    L18: b *= b;
    L19: b *= 19;
    L20: b *= 11;
    L21: d += 3;
    L22: d *= 22;
    L23: d += 9;
    L24: b += d;
    L25: 
        // IP += a
        if (a == 0)
            goto L26;
        if (a == 1)
            goto L27;
        cout << "L25: a = " << a << endl;
        throw logic_error("Unknown offset at L25");
    L26: goto L1; // goto L1
    L27: d = 27;
    L28: d *= 28; // d*=28
    L29: d += 29; // d+=29
    L30: d *= 30; // d*=30
    L31: d *= 14;
    L32: d *= 32; // d*=32
    L33: b += d;
    L34: a = 0;
    L35: goto L1;
}

// A much faster version:
//
// Calculates the sum of the divisors of the given number.
// Apparently that's what my puzzle input does.
//
// To do this I had to reverse engineer the code and then spot
// the slow part and optimise it. I'm not sure if that's how it's
// meant to be done, but that's how I did it.
//
// In fact, once I spotted what it was doing, it was just a matter of
// running `factor 10551311` in bash and adding that along with 1 and
// 10551311 itself.
long sum_of_divisors(long number) {
    // Optimised: was two loops checking all pairs of numbers from 1 to b.
    // There's no point looping all the way up to b when we know it won't divide.
    // Instead, check all numbers from 1 to sqrt(b) to see if they're a factor.
        
    long sum = 0;
    for (long n = 1; n * n <= number; n++)
        if (number % n == 0)
            sum += n + number / n;

    return sum;
}

int main() {
    auto prog = time_travel_cpu::read_program("input/p19.txt");
    {
        time_travel_cpu::cpu proc;
        proc.run(prog);
        proc.show_registers();
    }
    return 0;

    cout << "Program listing:" << endl;
    for (int i = 0; i< prog.instructions.size(); i++) {
        auto& ixn = prog.instructions[i];
        cout << i << ": " << ixn.op_name << " " << ixn.a << " " << ixn.b << " " << ixn.c << endl;
    }
    cout << "-------------" << endl << endl;

    // {
    //     cpu proc;
    //     proc.run(prog);
    //     proc.show_registers();
    // }

    // {
    //     cpu proc;
    //     proc.set_register(0, 1);
    //     proc.run(prog);
    //     proc.show_registers();
    // }

    cout << sum_of_divisors(911) << endl;
    cout << sum_of_divisors(10551311) << endl;
}
