#include "common.hpp"
#include "coord.hpp"

using namespace std;

struct room {
    int min_steps_from_origin = -1;
    bool n = false, e = false, s = false, w = false;
};

// A 2D array of `room`s centred on (0, 0) with a given size in the X and Y dimensions.
struct room_map {
    vector<room> rooms;
    int radius;

    room_map(int radius): 
        rooms(radius * 2 * radius * 2), radius(radius) 
    {}

    room& operator[](coord c) {
        assert(c.x < radius);
        assert(c.y < radius);
        assert(c.x >= -radius);
        assert(c.y >= -radius);
        return rooms[(c.y + radius) * radius * 2 + (c.x) + radius];
    }

    auto begin() { return rooms.begin(); }
    auto end() { return rooms.end(); }
};

// The process of mapping out the facility is modelled as a set of "scouts" which follow the instructions
// and split up where necessary.
//
// Each one remembers its branch points so that once a "|" is reached it can start a new scout from where it
// branched off from; this avoids having to store any of the regex.
//
// The < and == operators are implemented to allow sort() and unique() to work which avoids
// having duplicate scouts (which would otherwise kill performance).
struct scout {
    // Where the scout is    
    coord pos;

    // A stack of branch points.
    vector<coord> branch_points;

    int depth() const { return branch_points.size(); }

    bool operator<(scout const& other) {
        if (pos < other.pos)
            return true;
        if (other.pos < pos)
            return false;
        return depth() < other.depth();
    }

    bool operator==(scout const& other) {
        if (pos != other.pos)
            return false;
        return depth() == other.depth();
    }
};

class facility {
public:
    room_map _rooms;

    // Assume 200x200 rooms is enough space to map it out.
    // We'll get an assertion if not.
    facility(): _rooms(100) {}

    // Map out the facility based on a regex.
    void build_from_regex(string_view regex, coord start) {
        vector<scout> scouts;
        int depth = 0;
        scouts.push_back({start});
        _rooms[start].min_steps_from_origin = 0;

        for (char c: regex) {
            switch(c) {
                // NESW: All scouts which are at the current level (i.e. not in a different branch)
                // should follow this instruction and set the door flags in the appropriate rooms.
                case 'E': 
                    for (auto& s: scouts) {
                        if (s.depth() == depth) {
                            _rooms[s.pos].e = true;
                            s.pos.x += 1;
                        }
                    }
                    break;
                case 'S':
                    for (auto& s: scouts) {
                        if (s.depth() == depth) {
                            _rooms[s.pos].s = true;
                            s.pos.y += 1;
                        }
                    }
                    break;
                case 'N':
                    for (auto& s: scouts) {
                        if (s.depth() == depth) {
                            _rooms[s.pos].n = true;
                            s.pos.y -= 1;
                        }
                    }
                    break;
                case 'W':
                    for (auto& s: scouts) {
                        if (s.depth() == depth) {
                            _rooms[s.pos].w = true;
                            s.pos.x -= 1;
                        }
                    }
                    break;
                case '(':
                    // Go down a level and bring all the current scouts with us
                    for (auto& s: scouts)
                        if (s.depth() == depth)
                            s.branch_points.push_back(s.pos);
                    depth++;
                    break;

                case '|': {
                    // TODO Duplicate all scouts at the current depth
                    int l = scouts.size();
                    for (int i = 0; i < l; i++) {
                        if (scouts[i].depth() == depth) {
                            auto new_scout = scouts[i];
                            new_scout.pos = new_scout.branch_points.back();
                            scouts[i].branch_points.pop_back();

                            if (find_if(scouts.begin(), scouts.end(), [&](auto& s2) {
                                return s2.pos == new_scout.pos && s2.depth() == new_scout.depth();
                            }) == scouts.end());
                                scouts.push_back(new_scout);
                        }
                    }

                    // After branching, ensure we don't have any duplicate scouts.
                    sort(scouts.begin(), scouts.end());
                    scouts.erase(unique(scouts.begin(), scouts.end()), scouts.end());
                    break;
                }
                case ')':
                    // Go up a level and ascend all scouts at the current depth to keep them in line with the new depth.
                    for (auto& s: scouts)
                        if (s.depth() == depth)
                            s.branch_points.pop_back();
                    depth--;
                    break;
            }
        }
    }

    // Try going in a particular direction to see if we can find a shorter path.
    // If we do, carry on exploring in all directions from that position.
    void explore(coord pos, coord next_pos) {
        auto& room = _rooms[pos];
        auto& next_room = _rooms[next_pos];

        if (next_room.min_steps_from_origin == -1 || next_room.min_steps_from_origin > room.min_steps_from_origin + 1) {
            // cout << "Found shorter path to " << next_pos << endl;
            next_room.min_steps_from_origin = room.min_steps_from_origin + 1;
            calculate_shortest_paths(next_pos);
        }
    }

    // Find the shortest path to each room by recursively exploring each room.
    void calculate_shortest_paths(coord pos) {
        auto& room = _rooms[pos];

        if (room.n)
            explore(pos, pos + coord(0, -1));
        if (room.s)
            explore(pos, pos + coord(0, 1));
        if (room.e)
            explore(pos, pos + coord(1, 0));
        if (room.w)
            explore(pos, pos + coord(-1, 0));
    }
};

int find_furthest_room_in_facility(const string_view regex, int expected) {
    facility f;
    coord origin(0, 0);
    f.build_from_regex(regex, origin);
    f.calculate_shortest_paths(origin);

    // Note that rooms which were never mapped out will have min_steps_from_origin == -1.
    // This happens to work well in this comparison.
    auto furthest = max_element(f._rooms.begin(), f._rooms.end(), [](auto r1, auto r2) { 
        return r1.min_steps_from_origin < r2.min_steps_from_origin;
    });
    cout << "Furthest room is " << furthest->min_steps_from_origin << " doors away (expected " << expected << ")" << endl;

    auto further_than_1000 = count_if(f._rooms.begin(), f._rooms.end(), [](auto& r) {
        return r.min_steps_from_origin >= 1000;
    });
    cout << "There are " << further_than_1000 << " rooms at least 1000 doors away" << endl;
}

int main() {
    string regex;
    getline(ifstream("input/p20.txt"), regex);

    find_furthest_room_in_facility("^WNE$", 3);
    find_furthest_room_in_facility("^ENWWW(NEEE|SSE(EE|N))$", 10);
    find_furthest_room_in_facility("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", 18);
    find_furthest_room_in_facility("^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$", 23);
    find_furthest_room_in_facility("^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$", 31);

    find_furthest_room_in_facility(regex, -1);
}
