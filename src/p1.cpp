#include "./common.hpp"

int main() {
    std::set<int> seen;
    int frequency = 0;
    bool seen_duplicate = false;

    for (int i = 0; !seen_duplicate; i++) {
        std::ifstream input_file("input/p1.txt");
        while (!input_file.eof()) {
            int delta;
            input_file >> delta;
            frequency += delta;

            if (!seen_duplicate) {
                if (seen.find(frequency) == seen.end()) {
                    seen.insert(frequency);
                } else {
                    seen_duplicate = true;
                    std::cout << "First duplicate frequency is " << frequency << std::endl;
                }
            }
        }

        if (i == 0)
            std::cout << "Resulting frequency is " << frequency << std::endl;
    }
}

