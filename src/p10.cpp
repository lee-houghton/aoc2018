#include "common.hpp"

using namespace std;

struct star {
    int x = 0, y = 0, vx = 0, vy = 0;
};

auto parse_input(const char* path) {
    vector<star> stars;
    ifstream input(path);
    regex rx("position=<\\s*([\\-\\d]+),\\s*([\\-\\d]+)>\\s*velocity=<\\s*([\\-\\d]+),\\s*([\\-\\d]+)>", regex_constants::ECMAScript);
    while (!input.eof()) {
        string line;
        getline(input, line);
        if (line.length() > 0) {
            smatch m;
            if (!regex_match(line, m, rx))
                throw invalid_argument(line);
            stars.push_back({ stoi(m[1]), stoi(m[2]), stoi(m[3]), stoi(m[4]) });
        }
    }
    return stars;
}

int main() {
    auto stars = parse_input("input/p10.txt");
    long last_width = numeric_limits<long>::max(), last_height = numeric_limits<long>::max();
    long last_minX, last_minY;
    int seconds = 0;

    while (true) {
        // Move the stars
        for (auto& star: stars) {
            star.x += star.vx;
            star.y += star.vy;
        }

        // Find new bounds
        long minX = min_element(stars.begin(), stars.end(), [](auto& s1, auto& s2) { return s1.x < s2.x; })->x;
        long maxX = max_element(stars.begin(), stars.end(), [](auto& s1, auto& s2) { return s1.x < s2.x; })->x;
        long minY = min_element(stars.begin(), stars.end(), [](auto& s1, auto& s2) { return s1.y < s2.y; })->y;
        long maxY = max_element(stars.begin(), stars.end(), [](auto& s1, auto& s2) { return s1.y < s2.y; })->y;
        long width = maxX - minX + 1, height = maxY - minY + 1;
        
        long area = (maxX - minX) * (maxY - minY);

        // Stop when either the width or height stops decreasing and starts increasing
        if (width > last_width || height > last_height) {
            // Draw the message into a temporary string
            string message;
            message.reserve((last_width + 1) * last_height);

            // Fill the string with spaces and line breaks
            for (int y = 0; y < last_height; y++) {
                for (int x = 0; x < last_width; x++)
                    message += ' ';
                message += '\n';
            }

            // Draw *s where the stars are
            for (auto& star: stars) {
                auto x = star.x - star.vx - last_minX;
                auto y = star.y - star.vy - last_minY;
                message[(last_width + 1) * y + x] = '*';
            }
            
            cout << message;
            cout << "That took " << seconds << " seconds" << endl;
            return 0;
        }
        
        last_width = width;
        last_height = height;
        last_minX = minX;
        last_minY = minY;
        seconds++;
    }
}
