#include "common.hpp"

using namespace std;

struct node {
    vector<int> metadata;
    vector<shared_ptr<node>> children;
};

template<typename II>
auto read_header(II& begin, II end) {
    if (begin == end)
        throw logic_error("read error 1");
    int children = *begin++;
    if (begin == end)
        throw logic_error("read error 1a");
    int metadata = *begin++;
    return tuple(children, metadata);
}

template<typename II>
int compute_metadata_sum(II& it, II end) {
    auto [children, metadata] = read_header(it, end);

    int total = 0;
    for (int i = 0; i < children; i++)
        total += compute_metadata_sum(it, end);
    
    for (int i = 0; i < metadata; i++) {
        if (it == end)
            throw logic_error("read error 2");
        total += *it++;
    }

    return total;
}

template<typename II>
int compute_node_value(II& it, II end) {
    auto [children, metadata] = read_header(it, end);

    int value = 0;

    if (children == 0) {
        // Sum the metadata values 
        while (metadata --> 0) {
            if (it == end)
                throw logic_error("read error 3");
            value += *it++;
        }
    } else {
        // Sum the values of the child nodes referenced by the metadata values
        // (Where metadata value 1 references the first child node)
        vector<int> child_values;

        for (int i = 0; i < children; i++)
            child_values.push_back(compute_node_value(it, end));

        while (metadata --> 0) {
            if (it == end)
                throw logic_error("read error 3b");
            const int index = *it++;
            if (index >= 1 && index <= children)
                value += child_values.at(index - 1);
        }
    }

    return value;
}

int main() {
    vector<int> numbers;
    
    {
        ifstream input("input/p8.txt");
        istream_iterator<int> in(input);
        cout << "Metadata sum: " << compute_metadata_sum(in, istream_iterator<int>()) << endl;
    }

    {
        ifstream input("input/p8.txt");
        istream_iterator<int> in(input);
        cout << "Root node value: " << compute_node_value(in, istream_iterator<int>()) << endl;
    }
}
