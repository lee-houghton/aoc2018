#pragma once 

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <unordered_set>
#include <regex>
#include <string>
#include <queue>
#include <vector>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <fstream>
#include <sstream>
#include <thread>
#include <tuple>

inline auto read_lines(const char* path) {
    std::ifstream input(path);
    std::vector<std::string> lines;
    while (!input.eof()) {
        std::string line;        
        std::getline(input, line);
        if (!line.empty())
            lines.push_back(line);
        else 
            break;
    }
    return lines;
}
