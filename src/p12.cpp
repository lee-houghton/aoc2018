#include "common.hpp"

using namespace std;

string example_state = "#..#.#..##......###...###";
map<string, char> example_rules = { 
    { "...##", '#' },
    { "..#..", '#' },
    { ".#...", '#' },
    { ".#.#.", '#' },
    { ".#.##", '#' },
    { ".##..", '#' },
    { ".####", '#' },
    { "#.#.#", '#' },
    { "#.###", '#' },
    { "##.#.", '#' },
    { "##.##", '#' },
    { "###..", '#' },
    { "###.#", '#' },
    { "####.", '#' } 
};

string initial_state = "#..######..#....#####..###.##..#######.####...####.##..#....#.##.....########.#...#.####........#.#.";
map<string, char> rules = {
    { "#...#", '.' },
    { "##.##", '.' },
    { "###..", '.' },
    { ".####", '.' },
    { "#.#.#", '.' },
    { "##..#", '#' },
    { "..#.#", '#' },
    { ".##..", '#' },
    { "##...", '#' },
    { "#..##", '#' },
    { "#..#.", '.' },
    { ".###.", '#' },
    { "#.##.", '.' },
    { "..###", '.' },
    { ".##.#", '#' },
    { "....#", '.' },
    { "#####", '.' },
    { "#.###", '.' },
    { ".#..#", '.' },
    { "#....", '.' },
    { "...##", '.' },
    { ".#.##", '.' },
    { "##.#.", '#' },
    { "#.#..", '#' },
    { ".....", '.' },
    { ".#...", '#' },
    { "...#.", '#' },
    { "..#..", '.' },
    { "..##.", '.' },
    { "###.#", '.' },
    { "####.", '.' },
    { ".#.#.", '.' }
};

/**
 * Simulates the propagation of the plants in the infinite line of pots,
 * which is pretty much a cellular automata.
 * 
 * The first part is very simple, just simulate the state and calculate
 * the required answer at the end of the 20 iterations. The only tricky
 * part is that the state may expand in both directions, so we use the 
 * `offset` value to keep track of what plant pot number state[0] refers to.
 * 
 * So, initially, offset = 0 and state = the puzzle input, and for each cell
 * we look at the cell from the previous generation and the 2 cells to either
 * side to figure out what the next one will be. We know that the first and 
 * last 2 cells in the new state will be '.' because of the rules and the fact
 * that we've ensured the state is padded with 5 '.' on either side. When 
 * calculating the answer, we just need to take into account the offset when
 * adjusting the plant pot numbers.
 * 
 * Part 2 is very much more difficult. 50 billion iterations is way too much to
 * simulate, so we need to take advantage of the fact that the pattern of states
 * repeats, except each time it repeats it is shifted further to the right (or left).
 * 
 * To account for this shifting, we need to "canonicalise" the state so that it
 * always has the same amount of "..." padding, in this case we use 5 dots. This
 * allows us to keep track (in `seen_states`) of what states we have previously seen,
 * and in which generation. We also need to keep track of the offset we had at the
 * time that state was seen.
 * 
 * Once we see a state twice, we know that the pattern repeats. We can then figure out
 * the periodicity of the pattern, which will be the number of iterations that has taken
 * place since the first instance of this pattern. We can then project forward to see
 * what the offset will be at 50 billion iterations. The first repeated state we see many
 * not be useful, as projecting forward might end up at a different number like 50000000012
 * for example. We may need to wait until we hit the state that will be seen at 50 billion
 * iterations.
 * 
 * Once we hit the state which will repeat at 50 billion iterations, we can then figure out
 * how far the offset shifts every iteration (1, in my case) and figure out what the offset
 * will be at 50 billion iterations, at which point we can "fast forward" to that offset
 * do the calculations as normal.
 */
void simulate(string const& initial_state, map<string, char> const& rules, long generations) {
    long offset = 0;
    string state = initial_state;
    map<string, int> seen_states;
    vector<long> offsets = { 0 };

    cout << "Simulating for " << generations << " generations" << endl;

    for (long i = 1; i <= generations; i++) {
        auto first = find(state.begin(), state.end(), '#');
        assert(first != state.end());

        // Ensure the correct amount of "..." at the beginning/end.
        // This is needed to "canonicalise" the state so that it can
        // be stored and looked up in `seen_states`.
        if (first >= state.begin() + 5) {
            // Too many dots... the pattern has shifted right. 
            // Remove some dots and increment the offset
            offset += distance(state.begin() + 5, first);
            state.erase(state.begin(), first - 5);
        } else {
            // Not enough dots... the pattern has shifted left.
            // Add some dots and decrease the offset.
            int n = distance(first, state.begin() + 5);
            offset -= n;
            state = string(n, '.') + state;
        }

        // Ensure that there's always 5 dots at the end.
        // It just happened that my pattern never contracts at the right edge,
        // another rule set might require extra logic here.
        auto last = find(state.rbegin(), state.rbegin() + 5, '#');
        if (last != state.rbegin() + 5)
            state += ".....";

        // Calculate the new state by simulating the cellular automaton using 
        // the given rules map.
        string new_state = state;
        for (int j = 2; j < state.length() - 3; j++) {
            string env = state.substr(j - 2, 5);

            auto it = rules.find(env);
            new_state[j] = it != rules.end() ? it->second : '.'; 
        }

        // Keep track of the offset for this generation.
        offsets.push_back(offset);

        auto it = seen_states.find(new_state);
        if (it != seen_states.end()) {
            // This state has been seen before.
            auto prev_generation = it->second;
            auto gens = i - prev_generation; // How many generations has passed since this state was last seen.
            auto diff = offsets[i] - offsets[prev_generation]; // How far the pattern has shifted rightwards or leftwards.

            cout << "Found a repeated state at generations " << prev_generation << " and " << i << " (pattern has shifted by " << diff << " in " << gens << " generations)" << endl;

            // Will this pattern repeat at exactly the required generation number?
            if ((generations - i) % diff == 0) {
                // If so, calculate the new offset
                auto shift = (generations - i) * diff / gens;
                cout << "Found the final state, stepping ahead " << (generations - i) << " generations -- shifting offset by " << shift << endl;
                offset += shift;
                cout << "Offset is now " << offset << endl;

                // Don't forget to set the new state, or we'll have an off-by-one error.
                state = new_state;
                break;
            } 
        } else {
            // This is the first time this state has been seen, log it.
            seen_states.insert(it, pair(new_state, i));
        }

        state = new_state;
    }

    // Calculate the puzzle answer
    long total = 0;
    for (int i = 0; i < state.length(); i++) {
        if (state[i] == '#')
            total += (i + offset);
    }

    cout << "Total is " << total << endl;
}

int main() {
    // simulate(example_state, example_rules, 20);
    simulate(initial_state, rules, 20);
    simulate(initial_state, rules, 50000000000l);
}

