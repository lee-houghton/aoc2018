#include "common.hpp"
#include "grid.hpp"

using namespace std;

struct vein {
    int min_x, min_y, max_x, max_y;
};

auto read_input(char const* path) {
    ifstream input(path);
    vector<vein> veins;
    while (!input.eof()) {
        string line;
        getline(input, line);
        if (line.empty())
            continue;
        int c1, c2, c3;
        bool is_x = line[0] == 'x';
        replace_if(line.begin(), line.end(), [](char c) { return !isdigit(c); }, ' ');
        stringstream(line) >> c1 >> c2 >> c3;
        if (is_x)
            veins.push_back({ c1, c2, c1, c3 });
        else 
            veins.push_back({ c2, c1, c3, c1 });
    }

    
    return veins;
}

auto build_grid(vector<vein> const& veins) {
    auto min_x = min_element(veins.begin(), veins.end(), [](auto a, auto b) { return a.min_x < b.min_x; })->min_x - 1;
    auto max_x = max_element(veins.begin(), veins.end(), [](auto a, auto b) { return a.max_x < b.max_x; })->max_x + 1;
    auto min_y = min_element(veins.begin(), veins.end(), [](auto a, auto b) { return a.min_y < b.min_y; })->min_y;
    auto max_y = max_element(veins.begin(), veins.end(), [](auto a, auto b) { return a.max_y < b.max_y; })->max_y;
    // cout << min_x << " " << min_y << endl;
    // cout << max_x << " " << max_y << endl;
    grid<char> g(max_x - min_x + 3, max_y - min_y + 1);
    g.fill('.');
    for (auto& v: veins) {
        for (int y = v.min_y; y <= v.max_y; y++) {
            for (int x = v.min_x; x <= v.max_x; x++) {
                g.at(x - min_x, y - min_y) = '#';
            }
        }
    }
    int spring_x = 500 - min_x, spring_y = 0 - min_y;
    if (spring_y < 0)
        spring_y = 0;
    g.at(spring_x, spring_y) = '+';
    return tuple(g, spring_x, spring_y);
}

bool is_solid(char glyph) {
    switch(glyph) {
        case '#': case '~':
            return true;
        case '.': case '|': case '+': default:
            return false;
    }
}

bool is_water(char glyph) {
    return glyph == '~' || glyph == '|';
}

enum droplet_fate {
    WALL = 1,
    VOID = 2,
    REST = 3,
    DROP = 4,
};

droplet_fate simulate_droplet(grid<char>& g, int x, int y, int dx, int dy) {
    if (x < 0 || x >= g.width() || y >= g.height())
        return VOID;

    if (is_solid(g.at(x, y)))
        return WALL;

    if (y == g.height() - 1) {
        g.at(x, y) = '|';
        return VOID;
    }

    char below = g.at(x, y + 1);
    if (dx == 0) {
        if (g.at(x, y) == '.')
            g.at(x, y) = '|';

        if (!is_solid(below)) {
            simulate_droplet(g, x, y + 1, 0, 1);
            below = g.at(x, y + 1);
        }

        if (is_solid(below)) {
            auto left = simulate_droplet(g, x - 1, y, -1, 0);
            auto right = simulate_droplet(g, x + 1, y, +1, 0);

            if (left == WALL && right == WALL) {
                for (int xx = x - 1; !is_solid(g.at(xx, y)); xx--)
                    g.at(xx, y) = '~';
                for (int xx = x + 1; !is_solid(g.at(xx, y)); xx++)
                    g.at(xx, y) = '~';
                g.at(x, y) = '~';

                return REST;
            } else {
                g.at(x, y) = '|';
                return WALL;
            }
        } else {
            g.at(x, y) = '|';
            return DROP;
        }
    } else {
        if (g.at(x, y) == '.')
            g.at(x, y) = '|';

        // Used to be if(!is_solid(below)), however that is much slower, because it can result
        // in duplicate work if streams of water split up and merge together again (exponentially
        // in the number of merges).
        //
        // Changing this single line of code resulted in a speedup by a factor of 1000.
        if (below == '.') {
            simulate_droplet(g, x, y + 1, 0, 1);
            below = g.at(x, y + 1);
        }

        if (is_solid(below))
            return simulate_droplet(g, x + dx, y, dx, dy);
        else
            return DROP;
    }
}

int main() {
    auto veins = read_input("input/p17.txt");
    auto [grid, sx, sy] = build_grid(veins);
    simulate_droplet(grid, sx, sy, 0, 1);
    auto watery_squares = count_if(grid.begin(), grid.end(), is_water);
    auto still_water = count(grid.begin(), grid.end(), '~');
    cout << watery_squares << " squares are reachable by water" << endl;
    cout << still_water << " squares are retained" << endl;
}
