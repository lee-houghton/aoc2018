#include "common.hpp"
#include "iter.hpp"

using namespace std;

constexpr int required_recipe_count = 765071;
constexpr int required_score_count = 10;

void debug(vector<int>& scoreboard, int elf1, int elf2) {
    for (int i = 0; i < scoreboard.size(); i++) {
        if (i == elf1)
            cout << "(" << scoreboard[i] << ") ";
        else if (i == elf2)
            cout << "[" << scoreboard[i] << "] ";
        else 
            cout << scoreboard[i] << " ";
    }
    cout << endl;
}

int main() {
    bool part1 = false, part2 = false;
    int part2_start = 0;
    vector<int> scoreboard = { 3, 7 };
    vector<int> search;
    for (int x = required_recipe_count; x; x /= 10)
        search.insert(search.begin(), x % 10);
    
    // Keep track of the elf locations in the scoreboard.
    // Note that list<T>::iterator does not get invalidated when adding or removing elements,
    // which is a behaviour which we rely on.
    int elf1 = 0, elf2 = 1;

    // debug(scoreboard, elf1, elf2);

    // Run the algorithm until we have at least (puzzle_input + 10) recipe scores.
    while (!(part1 && part2)) {
        // Get the new score
        auto new_recipe_score = scoreboard[elf1] + scoreboard[elf2];
        if (new_recipe_score >= 10)
            scoreboard.push_back(new_recipe_score / 10);

        // Add new score(s) and increment the count
        scoreboard.push_back(new_recipe_score % 10);
        
        elf1 = (elf1 + (1 + scoreboard[elf1])) % scoreboard.size();
        elf2 = (elf2 + (1 + scoreboard[elf2])) % scoreboard.size();
        // debug(scoreboard, elf1, elf2);

        if (!part1) {
            if (scoreboard.size() >= required_recipe_count + required_score_count) {
                part1 = true;
                ostream_iterator<int> scores(cout);
                auto start = scoreboard.end();
                advance(start, required_recipe_count - scoreboard.size());
                copy(start, next(start, required_score_count), scores);
                cout << endl;
            }
        }

        if (!part2) {
            auto it = std::search(scoreboard.begin() + part2_start, scoreboard.end(), search.begin(), search.end());
            if (it != scoreboard.end()) {
                part2 = true;
                cout << "Found " << required_recipe_count << " after " << (it - scoreboard.begin()) << " recipes" << endl;
            }
            part2_start = scoreboard.size() - search.size();
        }
    }
}
