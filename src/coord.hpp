#pragma once 

#include <ostream>

struct coord {
    int x = 0;
    int y = 0;

    coord(int x, int y): x(x), y(y) {}
    coord(coord const& other): x(other.x), y(other.y) {}
    coord& operator=(coord const& other) { 
        x = other.x;
        y = other.y; 
        return *this;
    }
};

bool operator<(coord a, coord b) {
    return a.y < b.y || (a.y == b.y && a.x < b.x);
}

bool operator==(coord a, coord b) {
    return a.x == b.x && a.y == b.y;
}

coord operator+(coord a, coord b) {
    return coord(a.x + b.x, a.y + b.y);
}

bool operator!=(coord a, coord b) {
    return a.x != b.x || a.y != b.y;
}

std::ostream& operator<< (std::ostream& os, coord c) {
    return os << '(' << c.x << ',' << c.y << ')';
}
