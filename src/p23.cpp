#include "common.hpp"

using namespace std;

struct nanobot {
    int x, y, z, r;

    bool in_range(const nanobot& other) const {
        auto dist = abs(other.x - x) + abs(other.y - y) + abs(other.z - z);
        return dist <= r;
    }
};

ostream& operator<<(ostream& os, const nanobot& b) {
    return os << "bot({" << b.x << "," << b.y << "," << b.z << "}, " << b.r << ")";
}

auto read_bots(const char* path) {
    vector<nanobot> bots;
    for (const string& line: read_lines(path)) {
        int x, y, z, r;
        istringstream(line) >> x >> y >> z >> r;
        bots.push_back({ x, y, z, r });
    }
    return bots;
}

int main() {
    // Roll out!
    auto bots = read_bots("input/p23.txt");
    auto& strongest = *max_element(bots.begin(), bots.end(), [](auto& a, auto& b) { return a.r < b.r; });
    cout << "Strongest bot is " << strongest << endl;
    cout << count_if(bots.begin(), bots.end(), [&strongest](const auto& b) { return strongest.in_range(b); }) << " in range" << endl;
}
