#include <vector>

namespace {
    template<typename T> struct spacer { static constexpr const char* value = " "; };
    template<> struct spacer<char> { static constexpr const char* value = ""; };
}

template<typename T>
class grid {
public:
    grid(int width, int height)
        : _width(width), _height(height), _data(width * height)
    {}

    grid(grid<T> const& other)
        : _width(other._width), _height(other._height), _data(other._data)
    {}

    grid(grid<T>&& other)
        : _width(other._width), _height(other._height), _data(std::move(other._data))
    {
        other._width = 0;
        other._height = 0;
    }

    grid<T>& operator=(grid<T> const& other) = delete;
    grid<T>& operator=(grid<T>&& other) {
        _width = other._width;
        _height = other._height;
        _data = std::move(other._data);

        if (&other != this)
            other._width = other._height = 0;
    };

    typename std::vector<T>::reference at(int x, int y) {

        assert(x >= 0);
        assert(x < width());
        assert(y >= 0);
        assert(y < height());
        return _data[y * _width + x];
    }

    typename std::vector<T>::const_reference at(int x, int y) const {
        assert(x >= 0);
        assert(x < width());
        assert(y >= 0);
        assert(y < height());
        return _data[y * _width + x];
    }

    int width() const {
        return _width;
    }

    int height() const {
        return _height;
    }

    int valid_index(int x, int y) {
        return x >= 0 && x < _width && y >= 0 && y < _height;
    }

    void fill(T value) {
        std::fill(_data.begin(), _data.end(), value);
    }

    std::optional<std::tuple<int, int>> find(T value) {
        for (int y = 0; y < _height; ++y)
            for (int x = 0; x < _width; ++x)
                if(at(x, y) == value)
                    return std::optional(std::tuple(x, y));

        return std::nullopt;
    }

    void debug(std::ostream& os = std::cout) {
        for (int y = 0; y < _height; ++y) {
            for (int x = 0; x < _width; ++x) {
                os << at(x, y) << spacer<T>::value;
            }            
            os << std::endl;
        }
    }

    auto begin() {
        return _data.begin();
    }

    auto end() {
        return _data.end();
    }

    auto begin() const {
        return _data.begin();
    }

    auto end() const {
        return _data.end();
    }

private:

    int _width, _height;
    std::vector<T> _data;
};
