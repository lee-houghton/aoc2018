#include "common.hpp"
#include "cpu.hpp"

using namespace std;
using namespace time_travel_cpu;

// This is the disassembled version of my puzzle input.
// 
// It is what appears to be a linear congruential pesudorandom number generator
// or at least something very similar, surrounded by a loop which waits for r3
// to equal r0.
// 
// The first question is to find what value of r0 results in the fewest number of 
// instructions executed, which is simple - just find out the first value of r3
// so that it will break after the first loop.
// 
// The second question is to find what value of r0 results in the most number of 
// instructions executed, which is also quite simple - just wait for the sequence of 
// numbers to start a cycle and then choose the last value of r3 before the sequence 
// repeats.
void problem_21(long r0 = 0) {
    long r1 = 0, r3 = 0;

    // Part 1 is answered on the first iteration.
    long i = 0;

    // Keep track of what numbers we've seen so we know when the sequence is repeating.
    unordered_set<long> seen;
    optional<long> first_repeated;

    do {
        int last = r3;
        r1 = r3 | 65536;
        r3 = 10373714;
        
        while(true) {
            r3 = r3 + (r1 & 255);
            r3 = r3 & 16777215;
            r3 = r3 * 65899;
            r3 = r3 & 16777215;
            if (r1 < 256) 
                break;

            r1 /= 256;
        }

        if (i == 0) {
            cout << "Part 1 answer: " << r3 << endl;
        } else if (first_repeated && r3 == *first_repeated) {
            cout << "Part 2 answer: " << last << endl;
            return;
        } else if (!first_repeated) {
            auto hint = seen.find(r3);
            if (hint == seen.end())
                seen.insert(hint, r3);
            else { 
                first_repeated = r3;
                cout << "First repeated number is " << r3 << endl;
            }
        }

        i++;
    } while (r3 != r0);
}

int main() {
    // I used this to disassemble the puzzle input into something more readable.
    auto prog = read_program("input/p21.txt");
    disassemble(prog, "output/p21.asm");

    problem_21(0);

    return 0;
}
