#include "./common.hpp"

using namespace std;

int main() {
    std::vector<std::string> ids;

    std::ifstream input("input/p2.big.txt");
    while (!input.eof()) {
        std::string id;
        std::getline(input, id);
        if (id.length() > 0)
            ids.push_back(id);
    }

    int twice = 0, thrice = 0;
    for (auto& id: ids) {
        std::vector<int> counts(256);
        for (unsigned char c: id)
            counts[c]++;
        if (find(counts.begin(), counts.end(), 2) != counts.end())
            twice++;
        if (find(counts.begin(), counts.end(), 3) != counts.end())
            thrice++;
    }

    std::cout << "Checksum is " << twice << "*" << thrice << " = " << (twice * thrice) << std::endl;

    // Another solution, but it's slightly slower
    // 
    // for (int i = 0; i < 20; i++) {
    //     set<string> subsets;
    //     for (auto& id: ids) {
    //         string s = id;
    //         s[i] = '_';
    //         auto j = subsets.find(s);
    //         if (j == subsets.end()) {
    //             subsets.insert(j, s);
    //         } else {
    //             cout << "Winner is " << s << endl;
    //             return 0;
    //         }
    //     }
    // }
    // return 0;

    // For each string index, try replacing that index with _ in all the strings
    // and sort it. If any are equal, they will be adjacent.
    //
    for (int i = 0; i < 20; i++) {
        vector<string> sub_ids = ids;
        for_each(sub_ids.begin(), sub_ids.end(), [i](string& x) {
            x[i] = '_';
        });
        sort(sub_ids.begin(), sub_ids.end());
        for (int j = 0; j < sub_ids.size() - 1; j++) {
            if (sub_ids[j] == sub_ids[j + 1]) {
                cout << "Similar box IDs match " << sub_ids[j] << endl;
                return 0;
            }
        }
    }

    return 0;
}
