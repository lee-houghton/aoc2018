#pragma once 

#include <memory>
#include <optional>
#include <queue>
#include <tuple>

template<typename StateT, typename CostT>
inline std::optional<std::tuple<StateT, CostT>> astar_search(
    StateT initial,
    const std::function<bool(const StateT&)>& is_goal,
    const std::function<CostT(const StateT&)>& estimate_cost,
    const std::function<
        void(const StateT&, const std::function<void(const StateT& state, CostT cost)>&)
    >& legal_move_generator
) {
    struct Node {
        StateT state;
        CostT cost;
        CostT estimated_total_cost;

        bool operator>(const Node& other) const {
            return estimated_total_cost > other.estimated_total_cost;
        }

        bool operator<(const Node& other) const {
            return state < other.state;
        }

        bool operator==(const Node& other) const {
            return state == other.state;
        }
    };

    std::priority_queue<Node, std::vector<Node>, std::greater<Node>> open;
    std::set<StateT> closed;
    open.push({ initial, 0, estimate_cost(initial) });

    while(!open.empty()) {
        Node current = open.top();
        open.pop();
        closed.insert(current.state);

        // std::cout << "Visiting " << current.state.x << "," << current.state.y << " with " << current.state.equipped << " (cost = " << current.cost << ")" << "[size = " << open.size() << "]" << std::endl;

        if (is_goal(current.state))
            return std::tuple(current.state, current.cost);

        legal_move_generator(current.state, [&](const StateT& next, CostT cost) {
            if (closed.find(next) == closed.end()) {
                // std::cout << "\tAdding " << next.x << "," << next.y << " with " << next.equipped << " (movement cost = " << cost << ")" << std::endl;
                open.push({ next, current.cost + cost, current.cost + cost + estimate_cost(next) });
            }
        });
    }

    return std::nullopt;
}