template<typename ContainerT>
void cyclic_advance(ContainerT& list, typename ContainerT::iterator& iter, int count) {
    while (count --> 0) {
        if (iter == list.end())
            iter = list.begin();
        ++iter;
    }
}

template<typename ContainerT>
void cyclic_retreat(ContainerT& list, typename ContainerT::iterator& iter, int count) {
    while (count --> 0) {
        if (iter == list.begin())
            iter = list.end();
        --iter;
    }
}
