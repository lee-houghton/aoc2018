#include "common.hpp"

using namespace std;

void react(string& stack) {
    while (stack.length() >= 2) {
        char c1 = stack[stack.length() - 2], c2 = stack.back();
        
        if (c1 != c2 && toupper(c1) == toupper(c2)) {
            stack.pop_back(); 
            stack.pop_back();
        } else {
            return;
        }
    }
}

int get_reacted_length(const char* path, const char skip = 0) {
    string stack;
    ifstream input("input/p5.txt");
    
    while (true) {
        char c = input.get();
        if (input.eof())
            break;
        if (tolower(c) != skip) {
            stack.push_back(c);
            react(stack);
        }
    }

    return stack.length();
}

int main() {
    cout << "Part 1: Final length with no skipping is " << get_reacted_length("input/p5.txt") << endl;

    char best = 0;
    int best_size = 100000;
    for (char c = 'a'; c <= 'z'; c++) {
        int length = get_reacted_length("input/p5.txt", c);
        if (length < best_size) {
            best = c;
            best_size = length;
        }
    }

    cout << "Best character to remove is " << best << " resulting in a size of " << best_size << endl;
}

