#include "common.hpp"

using namespace std;

struct cpu_state {
    static constexpr int num_registers = 4;
    int reg[num_registers] = { 0, 0, 0, 0 };

    cpu_state update(int r, int value) {
        assert(r >= 0);
        assert(r < num_registers);

        cpu_state result = *this;
        result.reg[r] = value;
        return result;
    }

    int get(int r) {
        assert(r >= 0);
        assert(r < num_registers);
        return reg[r];
    }

    bool operator==(cpu_state const& other) const {
        for (int i = 0; i < num_registers; i++)
            if (reg[i] != other.reg[i])
                return false;
        return true;
    }
};

ostream& operator <<(ostream& os, cpu_state const& s) {
    os << "[" << s.reg[0] << "," << s.reg[1] << "," << s.reg[2] << "," << s.reg[3] << "]";
}

struct problem {
    cpu_state before, after;
    int opcode, a, b, out;
};

vector<problem> read_problems(char const* path) {
    ifstream input(path);
    vector<problem> problems;
    problem p;
    while(!input.eof()) {
        string line;
        getline(input, line);
        if (line.empty())
            continue;
        switch(line[0]) {
            case 'B': case 'A': {
                string str = line.substr(9);
                auto& state = *(line[0] == 'B' ? &p.before : &p.after);
                replace(str.begin(), str.end(), ',', ' ');
                replace(str.begin(), str.end(), ']', ' ');
                stringstream(str) >> state.reg[0] >> state.reg[1] >> state.reg[2] >> state.reg[3];
                if (line[0] == 'A')
                    problems.push_back(p);
                break;
            }
            default:
                stringstream(line) >> p.opcode >> p.a >> p.b >> p.out;
                break;
        }
    }

    return problems;
}

auto addr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) + state.get(b));
}

auto addi(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) + b);
}

auto mulr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) * state.get(b));
}

auto muli(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) * b);
}

auto banr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) & state.get(b));
}

auto bani(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) & b);
}

auto borr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) | state.get(b));
}

auto bori(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) | b);
}

auto setr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a));
}

auto seti(int a, int b, int out, cpu_state state) {
    return state.update(out, a);
}

auto gtir(int a, int b, int out, cpu_state state) {
    return state.update(out, a > state.get(b) ? 1 : 0);
}

auto gtri(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) > b ? 1 : 0);
}

auto gtrr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) > state.get(b) ? 1 : 0);
}

auto eqir(int a, int b, int out, cpu_state state) {
    return state.update(out, a == state.get(b) ? 1 : 0);
}

auto eqri(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) == b ? 1 : 0);
}

auto eqrr(int a, int b, int out, cpu_state state) {
    return state.update(out, state.get(a) == state.get(b) ? 1 : 0);
}

constexpr int num_opcodes = 16;

template<typename Function>
struct named_instruction {
    string name;
    Function function;
    bool possible_opcodes[num_opcodes];
    bool solved = false;
    int opcode = -1;

    named_instruction(string name, Function function): name(name), function(function) {
        // Assume initially that any instruction could be any opcode
        fill_n(possible_opcodes, num_opcodes, true);
    }
    named_instruction(named_instruction const& other) = delete;
    named_instruction(named_instruction const&& other) = delete;
    named_instruction& operator=(named_instruction const& other) = delete;
};

typedef named_instruction<cpu_state(*)(int,int,int,cpu_state)> cpu_instruction;
cpu_instruction instructions[num_opcodes] = {
    named_instruction("addr"s, addr),
    named_instruction("addi"s, addi),
    named_instruction("mulr"s, mulr),
    named_instruction("muli"s, muli),
    named_instruction("banr"s, banr),
    named_instruction("bani"s, bani),
    named_instruction("borr"s, borr),
    named_instruction("bori"s, bori),
    named_instruction("setr"s, setr),
    named_instruction("seti"s, seti),
    named_instruction("gtir"s, gtir),
    named_instruction("gtri"s, gtri),
    named_instruction("gtrr"s, gtrr),
    named_instruction("eqir"s, eqir),
    named_instruction("eqri"s, eqri),
    named_instruction("eqrr"s, eqrr)
};

int main() {
    auto problems = read_problems("input/p16.part1.txt");

    int trivariates = 0;
    for (auto p: problems) {
        cout << "Before: " << p.before.reg[0] << " " << p.before.reg[1] << " " << p.before.reg[2] << " " << p.before.reg[3] << endl;
        cout << "Instructions: " << p.opcode << " " << p.a << " " << p.b << " " << p.out << endl;
        cout << "After: " << p.after.reg[0] << " " << p.after.reg[1] << " " << p.after.reg[2] << " " << p.after.reg[3] << endl;

        int count = 0;
        for (auto& instruction: instructions) {
            auto result = instruction.function(p.a, p.b, p.out, p.before);
            if (result == p.after)
                count++; // cout << "Match with " << instruction.name << ": " << result << " == " << p.after << endl;
            else 
                instruction.possible_opcodes[p.opcode] = false;
        }
        if (count >= 3)
            trivariates++;
    }

    cout << trivariates << " problems behaved like 3 or more opcodes" << endl;

    while(true) {
        bool solved = true;
        for (auto& instruction: instructions) {
            if (instruction.solved)
                continue;

            solved = false;
            cout << instruction.name << ":";
            int possibilities = 0;
            for (int i = 0; i < num_opcodes; i++) {
                if (instruction.possible_opcodes[i]) {
                    cout << " " << i;
                    instruction.opcode = i;
                    possibilities++;
                }
            }
            cout << endl;
            
            if (possibilities == 1) {
                cout << "Solved: opcode " << instruction.opcode << " is " << instruction.name << endl;

                // Now we know that none of the other instructions could be this opcode
                for (auto& ix: instructions)
                    ix.possible_opcodes[instruction.opcode] = false;

                instruction.solved = true;
            }
        }
        if (solved)
            break;
    }

    cout << "Solved all opcodes" << endl;

    cpu_instruction* opcodes[num_opcodes];
    for (auto& ix: instructions)
        opcodes[ix.opcode] = &ix;

    ifstream input("input/p16.part2.txt");
    cpu_state state;
    
    while (!input.eof()) {
        string line;
        getline(input, line);
        if (line.empty())
            continue;
        int opcode, a, b, c;
        stringstream(line) >> opcode >> a >> b >> c;
        state = opcodes[opcode]->function(a, b, c, state);
        cout << state << endl;
    }
}
