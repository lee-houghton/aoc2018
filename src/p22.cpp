#include "common.hpp"
#include "astar.hpp"
#include "coord.hpp"
#include "grid.hpp"

using namespace std;

enum tool {
    none,
    torch,
    climbing_gear,
};

enum region_type {
    rocky = 0,
    wet = 1,
    narrow = 2,
};

struct region {
    int erosion_level = 0;
    region_type type = rocky;
};

typedef grid<region> cave;

int get_geological_index(const cave& cave, coord target, int x, int y) {
    if (x == 0 && y == 0)
        return 0;
    if (x == target.x && y == target.y)
        return 0;
    if (y == 0)
        return x * 16807;
    if (x == 0)
        return y * 48271;
    
    return cave.at(x-1, y).erosion_level * cave.at(x, y-1).erosion_level;
}

auto get_erosion_level(const cave& cave, int depth, coord target, int x, int y) {
    auto gi = get_geological_index(cave, target, x, y);
    return (gi + depth) % 20183;
}

auto generate_cave(int depth, coord target) {
    auto size = max(target.x, target.y) * 2;
    cave c(size, size);

    for (int r = 0; r < size; r++) {
        for (int x = 0; x < r; x++)
            if (c.valid_index(x, r))
                c.at(x, r).erosion_level = get_erosion_level(c, depth, target, x, r);
        
        for (int y = 0; y < r; y++)
            if (c.valid_index(r, y))
                c.at(r, y).erosion_level = get_erosion_level(c, depth, target, r, y);
        
        if (c.valid_index(r, r))
            c.at(r, r).erosion_level = get_erosion_level(c, depth, target, r, r);
    }

    for (auto& r: c)
        r.type = static_cast<region_type>(r.erosion_level % 3);

    return c;
}

// Check if a tool is valid in a particular region.
bool valid_equipment(region_type type, tool t) {
    if (type == rocky && t == none)
        return false;
    if (type == wet && t == torch)
        return false;
    if (type == narrow && t == climbing_gear)
        return false;
    return true;
}

// Determine what equipment to use on the next square. If possible, use the same equipment,
// otherwise use a tool which is valid for both the current and next region type.
//
// Each pair of region types has only one valid tool.
tool change_equipment(tool current_tool, region_type current_type, region_type new_type) {
    if (valid_equipment(new_type, current_tool))
        return current_tool;

    for (auto t: { none, torch, climbing_gear }) {
        if (valid_equipment(new_type, t) && valid_equipment(current_type, t))
            return t;
    }
    
    assert(!"Shouldn't get here");
    return none;
}

// Do an A* (best-first) search to find the best path to the target.
int find_cost_to_dude(cave& c, coord target) {
    struct state {
        int x, y;
        tool equipped;

        bool operator==(state const& other) const {
            return x == other.x && y == other.y && equipped == other.equipped;
        }

        bool operator<(state const& other) const {
            if (x < other.x) return true;
            if (x > other.x) return false;
            if (y < other.y) return true;
            if (y > other.y) return false;
            if (equipped < other.equipped) return true;
            return false;
        }
    };

    state initial = { 0, 0, torch };

    auto result = astar_search<state, int>(
        initial,
        [&](const state& s) -> bool { return s.x == target.x && s.y == target.y && s.equipped == torch; },
        [&](const state& s) -> int { return abs(s.x - target.x) + abs(s.y - target.y); },
        [&](const state& s, auto add_step) -> void {
            auto current_type = c.at(s.x, s.y).type;

            for (int dx = -1; dx <= 1; dx++) {
                for (int dy = -1; dy <= 1; dy++) {
                    if (s.x + dx < 0 || s.y + dy < 0)
                        continue;
                    if (dx && dy || (!dx && !dy)) // Only horizontal/vertical is allowed
                        continue;

                    if (!c.valid_index(s.x + dx, s.y + dy))
                        throw range_error("Out of bounds");

                    auto new_type = c.at(s.x + dx, s.y + dy).type;
                    auto new_equipment = (s.x + dx == target.x && s.y + dy == target.y)
                        ? torch // Need to use the torch at the target
                        : change_equipment(s.equipped, current_type, new_type);
                    auto cost = new_equipment == s.equipped ? 1 : 8;

                    add_step({ s.x + dx, s.y + dy, new_equipment }, cost);
                }
            }
        }
    );

    if (result)
        return get<1>(*result);
    
    throw logic_error("No path to dude");
}

void show_cave(const cave& c) {
    for (int y = 0; y < c.height(); y++) {
        for (int x = 0; x < c.width(); x++)
            cout << ".=|"[c.at(x, y).erosion_level % 3];
        cout << endl;
    }
    cout << endl;
}

void find_dude(int depth, coord target) {
    auto cave = generate_cave(depth, target);

    int risk_level = 0;
    for (int y = 0; y <= target.y; y++)
        for (int x = 0; x <= target.x; x++)
            risk_level += static_cast<int>(cave.at(x, y).type);

    cout << "Risk level is " << risk_level << endl;

    auto cost = find_cost_to_dude(cave, target);
    cout << "Time taken to find the dude is " << cost << endl;
}

int main() {
    find_dude(510, coord(10, 10));
    find_dude(4002, coord(5, 746));
}
