#include "common.hpp"

using namespace std;

struct step {
    string name;
    int length;
    bool busy;
    vector<shared_ptr<step>> prev;
    vector<shared_ptr<step>> next;

    step(string name, int min_length): name(name), length(0), busy(false) {
        if (!name.empty())
            length = static_cast<int>(name[0] - 'A') + 1 + min_length;
    }
};

void finish(shared_ptr<step> task) {
    for (auto next: task->next)
        next->prev.erase(remove(next->prev.begin(), next->prev.end(), task), next->prev.end());
}

auto read_input(const char* input_path, int min_length = 0) {
    vector<shared_ptr<step>> done, todo;
    map<string, shared_ptr<step>> steps;

    for (auto line: read_lines(input_path)) {
        string _, before, after;
        stringstream(line) >> _ >> before >> _ >> _ >> _ >> _ >> _ >> after;

        auto& s1 = steps[before];
        if (!s1) {
            s1 = make_shared<step>(before, min_length);
            todo.push_back(s1);
        }

        auto& s2 = steps[after]; 
        if (!s2) {
            s2 = make_shared<step>(after, min_length);
            todo.push_back(s2);
        }

        s1->next.push_back(s2);
        s2->prev.push_back(s1);
    }

    sort(todo.begin(), todo.end(), [](auto& x, auto& y){ return x->name < y-> name; });

    return todo;
}

struct worker {
    shared_ptr<step> task;

    void assign(shared_ptr<step> task) {
        assert(!task->busy);
        this->task = task;
        task->busy = true;
    }

    // Returns true if the task is done
    bool work() {
        if (!idle())
            return (--task->length == 0);

        return false;
    }

    bool idle() {
        return !task || task->length == 0;
    }
};

void part_1(const char* input_path) {
    auto todo = read_input(input_path);

    while (!todo.empty()) {
        auto first = find_if(todo.begin(), todo.end(), [](auto& x){ return !x->busy && x->prev.empty(); });
        if (first == todo.end()) {
            cout << "Part 1: no tasks can be started" << endl;
            return;
        } 
        
        auto s = *first;
        cout << s->name;
        finish(s);
        todo.erase(first, first + 1);
    }

    cout << endl;
}

void part_2(const char* input_path, int worker_count, int min_length) {
    auto todo = read_input(input_path, min_length);
    vector<worker> elves(worker_count);

    int ticks = 0;
    while (!todo.empty()) {
        ticks++;
        cout << "T" << setfill('0') << setw(4) << ticks << setw(0) << ": ";

        // Assign all tasks before starting work
        for (auto& elf: elves) {
            if (elf.idle()) {
                auto first = find_if(todo.begin(), todo.end(), [](auto& x){ return !x->busy && x->prev.empty(); });
                if (first != todo.end())
                    elf.assign(*first);
            }

            cout << (elf.idle() ? "."s : elf.task->name);
        }

        for (auto& elf: elves) {
            if (elf.work()) {
                finish(elf.task);
                todo.erase(remove(todo.begin(), todo.end(), elf.task), todo.end());
            }
        }

        cout << endl;
    }

    cout << "Done in " << ticks << " ticks " << endl;
}

int main() {
    // part_1("input/p7.ex.txt");
    part_1("input/p7.txt");
    // part_2("input/p7.ex.txt", 2, 0);
    part_2("input/p7.txt", 5, 60);
}
