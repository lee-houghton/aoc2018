#include "common.hpp"

using namespace std;

struct rect {
    int top = 0, left = 0, bottom = 0, right = 0;
};

ostream& operator<<(ostream& os, rect const& r) {
    os << "{" << r.left << "," << r.top << ":" << r.right << "," << r.bottom << "}";
    return os;
}

struct coord {
    int x = 0, y = 0;
    int owned = 0;
    bool finite = true;
};

auto read_input() {
    vector<coord> coords;
    ifstream input("input/p6.txt");
    int id = 1;
    while (!input.eof()) {
        coord c = { id++ };
        input >> c.x;
        input.get();
        input >> c.y;
        coords.push_back(c);
    }
    return coords;
}

auto calculate_bounds(vector<coord>& coords) {
    rect bounds = { numeric_limits<int>::max(), numeric_limits<int>::max(), numeric_limits<int>::min(), numeric_limits<int>::min() };
    for (auto c = coords.begin(); c != coords.end(); c++) {
        if (c->x < bounds.left)
            bounds.left = c->x;
        if (c->x > bounds.right)
            bounds.right = c->x;
        if (c->y < bounds.top)
            bounds.top = c->y;
        if (c->y > bounds.bottom)
            bounds.bottom = c->y;
    }
    return bounds;
}

int distance(coord const& c, int x, int y) {
    return abs(c.x - x) + abs(c.y - y);
}

auto get_cell_info(vector<coord>& coords, int x, int y) {
    int closest_distance = numeric_limits<int>::max();
    int total_distance = 0;
    coord* closest;

    for (auto& c: coords) {
        const int dist = distance(c, x, y);
        total_distance += dist;
        if (dist < closest_distance) {
            closest_distance = dist;
            closest = &c;
        } else if (dist == closest_distance) {
            closest = nullptr; // It's a tie
        }
    }

    return tuple(closest, total_distance);
}

int main() {
    auto coords = read_input();
    rect bounds = calculate_bounds(coords);
    cout << "Bounds: " << bounds << endl;

    // Figure out which cell is nearest to which coordinate by doing a flood fill from each coordinate
    int safe_cells = 0;

    // Go through the grid finding the closest coordinate
    for (int y = bounds.top; y <= bounds.bottom; y++) {
        for (int x = bounds.left; x <= bounds.right; x++) {
            auto [closest, total] = get_cell_info(coords, x, y);
            if (closest) {
                if (x == bounds.left || x == bounds.right || y == bounds.top || y == bounds.bottom)
                    closest->finite = false;
                closest->owned++;
            }
            if (total < 10000)
                safe_cells++;
        }
    }

    auto largest_area = max_element(
        coords.begin(),
        coords.end(),
        [](auto c1, auto c2) {
            return (c1.finite ? c1.owned : 0) < (c2.finite ? c2.owned : 0);
        }
    );
    
    cout << "Largest area has " << largest_area->owned << " squares" << endl;
    cout << "There are " << safe_cells << " safe cells" << endl;
}
