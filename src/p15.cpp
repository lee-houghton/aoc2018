#include "common.hpp"
#include "grid.hpp"
#include "coord.hpp"

using namespace std;

struct unit {
    char type = '#';
    int health = 200;
    int attack = 0;
    bool moved = false;

    void damage(int amount) {
        assert(health > 0);
        health -= amount;
        if (health <= 0) {
            type = '.';
            health = 0;
            attack = 0;
            moved = false;
        }
    }

    void vacate() {
        assert(type == 'E' || type == 'G');
        assert(health > 0);
        type = '.';
        health = 0;
        attack = 0;
        moved = false;
    }

    unit& operator=(const unit& other) {
        type = other.type;
        health = other.health;
        attack = other.attack;
        moved = other.moved;
        return *this;
    }
};

class simulation: public grid<unit> {
    int elves, goblins, killed_elves;
public:
    simulation(int width, int height)
        : grid(width, height), elves(0), goblins(0), killed_elves(0)
    {}

    simulation(simulation const& other)
        : grid(other), elves(other.elves), goblins(other.goblins), killed_elves(other.killed_elves)
    {}

    void init(int elf_attack_power) {
        for (int y = 0; y < height(); y++) {
            for (int x = 0; x < width(); x++) {
                auto& unit = at(x, y);
                switch(unit.type) {
                    case 'E':
                        unit.attack = elf_attack_power;
                        elves++;
                        break;
                    case 'G':
                        unit.attack = 3;
                        goblins++;
                        break;
                }
            }
        }
    }

    void debug() {
        // cout << "There are " << elves << " elves and " << goblins << " goblins (" << killed_elves << " dead elves)" << endl;
        return;

        for (int y = 0; y < height(); y++) {
            for (int x = 0; x < width(); x++) {
                cout << at(x, y).type;
            }

            for (int x = 0; x < width(); x++) {
                auto& u = at(x, y);
                if (u.type == 'E' || u.type == 'G')
                    cout << ' ' << u.type << '(' << u.health << ')';
            }

            cout << endl;
        }
    }

    void reset_moves() {
        for (int y = 0; y < height(); y++)
            for (int x = 0; x < width(); x++)
                at(x, y).moved = false;
    }

    static simulation read_from_file(char const* path) {
        auto lines = read_lines(path);
        if (lines.size() == 0 || lines[0].empty())
            throw new range_error("Invalid map dimensions");

        simulation sim(lines[0].length(), lines.size());
        for (int y = 0; y < lines.size(); y++) {
            string& line = lines[y];
            for (int x = 0; x < line.length(); x++) {
                sim.at(x, y) = { line[x] };
            }
        }

        return sim;
    }

    optional<coord> find_adjacent_target(int x0, int y0, char target_type) {
        int best_hp = -1, best_x = -1, best_y = -1;

        for (coord move: { coord(x0, y0 - 1), coord(x0 - 1, y0), coord(x0 + 1, y0), coord(x0, y0 + 1) }) {
            if (!valid_index(move.x, move.y))
                continue;

            auto& found = at(move.x, move.y);
            if (found.type == target_type && found.health >= 0) {
                if (best_hp == -1 || found.health < best_hp) {
                    best_hp = found.health;
                    best_x = move.x;
                    best_y = move.y;
                }
            }
        }

        if (best_hp == -1)
            return nullopt;

        return optional(coord(best_x, best_y));
    }

    optional<coord> find_next_pos(int x0, int y0, char target_type) {
        struct state {
            coord pos;
            coord starting_move;
            bool operator<(const state& other) const { 
                return pos < other.pos || (pos == other.pos && starting_move < other.starting_move);
            }
            bool operator==(const state& other) const {
                return pos == other.pos && starting_move == other.starting_move;
            }
        };

        //set<state> closed;
        vector<state> closed;
        vector<state> next, current;

        // Set up initial moves and check for adjacent target
        for (coord move: { coord(x0, y0 - 1), coord(x0 - 1, y0), coord(x0 + 1, y0), coord(x0, y0 + 1) }) {
            if (!valid_index(move.x, move.y))
                continue;

            auto& found = at(move.x, move.y);
            if (found.type == target_type) {
                // Are we already next to a valid target?
                // Don't bother finding the one with lowest HP, the caller will do that.
                return optional(coord(x0, y0));
            } else if (found.type == '.') {
                state new_move = { move, move };
                current.push_back(new_move);
                closed.push_back(new_move);
            } else {
                // Found something else (e.g. # or a unit of the same type)
                // Don't consider this move any further
            }
        }
        
        for (int moves = 0; ; moves++) {
            // No more moves to explore - no targets found
            if (current.empty())
                return nullopt;

            sort(current.begin(), current.end());

            for (auto cur: current) {
                for (coord move: { coord(cur.pos.x, cur.pos.y - 1), coord(cur.pos.x - 1, cur.pos.y), coord(cur.pos.x + 1, cur.pos.y), coord(cur.pos.x, cur.pos.y + 1) }) {
                    if (!valid_index(move.x, move.y))
                        continue;

                    // Have we already reached this square from this starting direction?
                    state new_move = { move, cur.starting_move };
                    auto hint = find(closed.begin(), closed.end(), new_move);
                    if (hint != closed.end())
                        continue;
                    closed.push_back(new_move);

                    // What is on the square in question?
                    auto found_type = at(move.x, move.y).type;
                    if (found_type == target_type) {
                        // cout << "Unit at (" << x0 << "," << y0 << ") targets unit at (" << move.x << "," << move.y << ") by moving to (" << cur.starting_move.x << "," << cur.starting_move.y << ")" << endl;
                        // It's a valid target. Return the square we first moved to on our way here
                        return optional(cur.starting_move);
                    }

                    if (found_type == '.') // Don't go through walls (or other units)
                        next.push_back(new_move);
                }
            }

            swap(next, current);
            next.clear();
        }

        return coord(x0, y0);
    }

    bool move_unit(int x, int y) {
        auto& u = at(x, y);
        if (!(u.type == 'E' || u.type == 'G') || u.moved)
            return false;

        char target_type = u.type == 'E' ? 'G' : 'E';
        auto move_pos = find_next_pos(x, y, target_type);
        if (!move_pos)
            return false; // No moves possible

        u.moved = true;

        if (*move_pos != coord(x, y)) {
            move_from_to(coord(x, y), *move_pos);
        }

        // Now see what we can attack
        auto& new_unit = at(move_pos->x, move_pos->y);
        auto target_pos = find_adjacent_target(move_pos->x, move_pos->y, target_type);
        if (target_pos)
            attack(*move_pos, *target_pos);

        return true;
    }

    void set_attack_power(char unit_type, int attack) {
        for (int y = 0; y < height(); y++)
            for (int x = 0; x < width(); x++)
                if (at(x, y).type == unit_type)
                    at(x, y).attack = attack;
    }

    unit& at(coord c) {
        return at(c.x, c.y);
    }

    unit& at(int x, int y) {
        return grid::at(x, y);
    }

    void move_from_to(coord from, coord to) {
        at(to) = at(from);
        at(from).vacate();
    }

    void attack(coord from, coord to) {
        auto& victim = at(to);
        char type = victim.type;
        victim.damage(at(from).attack);

        if (victim.type == '.') {
            // The victim died :(
            if (type == 'E') {
                elves--;
                killed_elves++;
            } else if (type == 'G') {
                goblins--;
            }
        }
    }

    bool is_ended() {
        return elves == 0 || goblins == 0;
    }

    int elf_casualties() {
        return killed_elves;
    }

    void simulate_turn() {
        for (int y = 0; y < height(); y++) {
            for (int x = 0; x < width(); x++) {
                auto& u = at(x, y);
                if (u.type == 'E' || u.type == 'G')
                    move_unit(x, y);
            }
        }
    }

    void simulate_battle(bool stop_on_elf_casualty) {
        for (int moves_completed = 0; ; moves_completed++) {
            cout << "\tMove " << moves_completed + 1 << endl;
            reset_moves();
            simulate_turn();
            debug();

            // Battle ends when any unit finds no targets
            if (is_ended() || (stop_on_elf_casualty && elf_casualties() > 0)) {
                int sum = health_sum();
                cout << "\tFinished after " << moves_completed << " full moves" << endl;
                cout << "\tHealth sum of " << sum << " gives outcome of " << sum*moves_completed << endl;
                cout << "\t" << elf_casualties() << " elves died in the name of Christmas" << endl;
                return;
            }
        }
    }

    int health_sum() {
        int sum = 0;
        for (int y = 0; y < height(); y++) {
            for (int x = 0; x < width(); x++) {
                auto& u = at(x, y);
                if (u.type == 'E' || u.type == 'G')
                    sum += u.health;
            }
        }
        return sum;
    }
};

int part1(simulation const& initial_sim) {
    simulation sim(initial_sim);
    sim.init(3);

    sim.debug();
    sim.simulate_battle(false);
}

int part2(simulation const& initial_sim) {
    for (int elf_attack_power = 4; ; elf_attack_power++) {
        cout << "Trying elf attack power of " << elf_attack_power << endl;
        simulation sim(initial_sim);
        sim.init(elf_attack_power);
        sim.simulate_battle(true);
        if (sim.elf_casualties() == 0) {
            cout << "Elfs win with attack power of " << elf_attack_power << endl;
            break;
        }
    }
}

int main() {
    auto sim = simulation::read_from_file("input/p15.txt");
    part1(sim);
    part2(sim);
}
