#include "common.hpp"
#include "grid.hpp"

using namespace std;

constexpr int grid_size = 300;

void fill_grid(grid<int>& g, int grid_serial_number) {
    for (int y = 0; y < grid_size; y++)
        for (int x = 0; x < grid_size; x++)
            g.at(x, y) = ((x + 10) * y + grid_serial_number) * (x + 10) / 100 % 10 - 5;
}


/**
 * Compute the sum of each (`area_size`*`area_size`) subgrid in `cells`,
 * placing the results into `areas`. Each value in `areas` corresponds to the 
 * sum of the values in the (`area_size`*`area_size`) subgrid in `cells` which
 * has its top left cell at that (x, y) position.
 */
void compute_area_sums(grid<int>& cells, grid<int>& areas, int area_size) {
    for (int y = 0; y < areas.height(); y++) {
        for (int x = 0; x < areas.width(); x++) {
            for (int dx = 0; dx < area_size; dx++)
                for (int dy = 0; dy < area_size; dy++)
                    areas.at(x, y) += cells.at(x + dx, y + dy);
        }
    }
}

/**
 * Computes similarly to compute_area_sums, except it avoids doing the same calculation
 * multiple times. Each call to compute_area_sums() covers all the cells in the subgrid
 * that have not yet been covered. This happens once for every valid (x, y) in (areas).
 * 
 * compute_incremental_area_sums(c, a, 1)
 * *...
 * ....
 * ....
 * ....
 * 
 * compute_incremental_area_sums(c, a, 2)
 * .*..
 * **..
 * ....
 * ....
 * 
 * compute_incremental_area_sums(c, a, 3)
 * ..*.
 * ..*.
 * ***.
 * ....
 */
void compute_incremental_area_sums(grid<int>& cells, grid<int>& areas, int area_size) {
    for (int y = 0; y < areas.height() - area_size; y++) {
        for (int x = 0; x < areas.width() - area_size; x++) {
            for (int dx = 0; dx < area_size; dx++)
                areas.at(x, y) += cells.at(x + dx, y + area_size - 1);

            for (int dy = 0; dy < area_size; dy++)
                areas.at(x, y) += cells.at(x + area_size - 1, y + dy);
        }
    }
}

/**
 * Finds the maximum vaue in grid `g` and returns a tuple of (x, y, value).
 */
template<class T>
auto grid_find_max(grid<T>& g) {
    T best = T();
    int best_x = -1, best_y = -1;
    for (int y = 0; y < g.height(); y++) {
        for (int x = 0; x < g.width(); x++) {
            if (g.at(x, y) > best) {
                best_x = x;
                best_y = y;
                best = g.at(x, y);
            }
        }
    }
    return tuple(best_x, best_y, best);
}

int main() {
    grid<int> cells(grid_size, grid_size);
    fill_grid(cells, 5153);

    {
        grid<int> areas(grid_size - 2, grid_size - 2);
        compute_area_sums(cells, areas, 3);
        auto [x, y, _] = grid_find_max(areas);
        cout << "Best 3x3 fuel cell area is at " << x << "," << y << endl;
    }

    {
        grid<int> areas(grid_size, grid_size);

        int best_x, best_y, best_area_size, best_value = numeric_limits<int>::min();
        for (int area_size = 1; area_size <= grid_size; area_size++) {
            compute_incremental_area_sums(cells, areas, area_size);
            auto [x, y, value] = grid_find_max(areas);
            cout << "Area size " << area_size << ": best is at " << best_x << "," << best_y << " and is " << best_value << endl;
            if (value > best_value) {
                best_x = x;
                best_y = y;
                best_area_size = area_size;
                best_value = value;
            }
        }
        cout << "Best fuel cell area overall is at " << best_x << "," << best_y << " with size " << best_area_size << endl;
    }
}
