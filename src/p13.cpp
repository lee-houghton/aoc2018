#include "common.hpp"
#include "grid.hpp"

using namespace std;

struct cart {
    int x = 0;
    int y = 0;
    int dx = 0;
    int dy = 0;
    int turns = 0;
    bool alive = true;

    cart(int x, int y, int dx, int dy)
        : x(x), y(y), dx(dx), dy(dy)
    {}

    void follow_track(char c) {
        switch (c) {
            case '-':
                assert(dy == 0);
                break;

            case '|': 
                assert(dx == 0);
                break;

            case '\\':
                swap(dx, dy);
                break;

            case '/':
                swap(dx, dy);
                dx *= -1;
                dy *= -1;
                break;

            // Intersection, cart behaviour follows a pattern depending on
            // how many turns the cart has previously taken.
            case '+': {
                int temp;
                switch(turns++ % 3) {
                    case 0: 
                        // Turn left (anticlockwise)
                        temp = -dx;
                        dx = dy;
                        dy = temp;
                        break;
                    case 1:
                        // Go straight
                        break;
                    case 2:
                        // Turn right (clockwise)
                        temp = dx;
                        dx = -dy;
                        dy = temp;
                        break;
                }
            }
        }

        x += dx;
        y += dy;
    }
};

/**
 * Read the input file and produce a file containing:
 * 1. A grid<char> containing the positions of the rails
 * 2. A vector<cart> containing a list of all the carts and their starting positions/velocities
 * 3. A grid<bool> containing true where a cart is on that spot, and false otherwise.
 */
auto read_input(char const* path) {
    auto lines = read_lines(path);
    if (lines.empty())
        throw length_error("Input file is empty or doesn't exist");

    int width = max_element(lines.begin(), lines.end(), [](auto& x, auto& y) { return x.length() < y.length(); })->length();
    int height = lines.size();
    grid<char> map(width, height);
    grid<bool> carts_map(map.width(), map.height());
    vector<cart> carts;

    for (int y = 0; y < map.height(); y++) {
        string const& line = lines[y];

        for (int x = 0; x < line.length(); x++) {
            char c = line[x];
            switch(c) {
                case 'v':
                    carts.push_back(cart(x, y, 0, 1));
                    map.at(x, y) = '|';
                    carts_map.at(x, y) = true;
                    break;

                case '^':
                    carts.push_back(cart(x, y, 0, -1));
                    map.at(x, y) = '|';
                    carts_map.at(x, y) = true;
                    break;

                case '>':
                    carts.push_back(cart(x, y, 1, 0));
                    map.at(x, y) = '-';
                    carts_map.at(x, y) = true;
                    break;

                case '<':
                    carts.push_back(cart(x, y, -1, 0));
                    map.at(x, y) = '-';
                    carts_map.at(x, y) = true;
                    break;

                default:
                    map.at(x, y) = line[x];
                    break;
            }
        }
    }
    return tuple(map, carts, carts_map);
}

/**
 * Simulates a single tick of the world.
 * Moves each cart, checking to see if it crashed with another.
 * If it did, it removes both carts and returns the one which crashed first.
 */
optional<cart> simulate(grid<char>& map, grid<bool>& carts_map, vector<cart>& carts, int& alive_carts) {
    // Carts go in reading order (top to bottom, left to right).
    // There's not enough carts in the problem to worry about this being slow.
    sort(carts.begin(), carts.end(), [](auto& c1, auto& c2) {
        return (c1.y < c2.y) || (c1.y == c2.y && c1.x < c2.x);
    });

    optional<cart> crash;

    for (auto& cart: carts) {
        if (!cart.alive)
            continue;

        carts_map.at(cart.x, cart.y) = false;
        cart.follow_track(map.at(cart.x, cart.y));

        if (carts_map.at(cart.x, cart.y)) {
            // Collision! Remove both carts (well, all carts at this location)
            crash = cart;
            for (auto& victim: carts) {
                if (victim.x == cart.x && victim.y == cart.y) {
                    victim.alive = false;
                    alive_carts--;
                }
            }

            // This square is now devoid of carts
            carts_map.at(cart.x, cart.y) = false;
        } else {
            // No collision, move the cart to this spot
            carts_map.at(cart.x, cart.y) = true;
        }
    }

    return crash;
}

int main() {
    auto [map, carts, carts_map] = read_input("input/p13.txt");
    cout << "There are " << carts.size() << " carts." << endl;

    int i = 0, alive_carts = carts.size();
    while (alive_carts > 1) {
        i++;
        auto crash = simulate(map, carts_map, carts, alive_carts);
        if (crash) {
            cout << "Tick " << i << ": Collision at " << crash->x << "," << crash->y << endl;
        } 
    }

    auto last = find_if(carts.begin(), carts.end(), [](auto& c) { return c.alive; });
    if (last != carts.end()) {
        cout << "The position of the final cart is " << last->x << "," << last->y << endl;
    } else {
        cout << "All the carts have been removed" << endl;
    }
}
