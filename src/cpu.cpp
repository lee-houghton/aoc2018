
#include "common.hpp"
#include "cpu.hpp"

using namespace std;
using namespace time_travel_cpu;

namespace {
    const std::map<std::string, opcode> opcodes = {
        { "addr", addr },
        { "addi", addi },
        { "mulr", mulr },
        { "muli", muli },
        { "banr", banr },
        { "bani", bani },
        { "borr", borr },
        { "bori", bori },
        { "seti", seti },
        { "setr", setr },
        { "gtri", gtri },
        { "gtir", gtir },
        { "gtrr", gtrr },
        { "eqrr", eqrr },
        { "eqri", eqri },
        { "eqir", eqir },
    };
}

namespace time_travel_cpu {
    bool is_comparison(opcode op) {
        return op >= gtri && op <= eqir;
    }

    program read_program(const char* path) {
        program p;
        
        for (auto line: read_lines(path)) {
            if (line.length() < 4)
                continue;
            istringstream ss(line.substr(4));
            auto instr = line.substr(0, 4);
            if (instr == "#ip ") {
                ss >> p.ip_register;
            } else {
                instruction ixn = { opcodes.at(instr) };
                ixn.op_name = instr;
                ss >> ixn.a >> ixn.b >> ixn.c;
                p.instructions.push_back(ixn);
            }
        }

        return p;
    }

    void disassemble(const program& prog, const char* output_path) {
        ofstream tsm(output_path);
    
        int ip = 0;
        optional<instruction> last;

        for (auto ixn: prog.instructions) {
            tsm << "L" << ip << ": ";
            if (ixn.c == prog.ip_register) {
                switch (ixn.op) {
                    case seti: 
                        tsm << "goto L" << ixn.a + 1; break;
                    case addi: 
                        if (ixn.a == prog.ip_register) {
                            tsm << "goto L" << ip + ixn.b + 1; break;
                        } else {
                            tsm << "invalid addi";
                        }
                    case addr: {
                        int other = -1;
                        if (ixn.a == prog.ip_register)
                            other = ixn.b;
                        else if (ixn.b == prog.ip_register)
                            other = ixn.a;

                        if (other == -1) {
                            tsm << "invalid addr";
                        } else if(last && last->c == other && is_comparison(last->op)) {
                            tsm << "if r" << other << " then goto L" << ip + 2;
                        } else {
                            tsm << "ip += r" << ixn.a;
                        }

                        break;
                    }
                    default: 
                        tsm << "unknown(" << ixn.op_name << ")";
                }
                tsm << endl;
            } else {
                tsm << "r" << ixn.c << " = ";
                switch(ixn.op) {
                    case seti: tsm << ixn.a; break;
                    case setr: tsm << "r" << ixn.a; break;

                    case gtri: tsm << "r" << ixn.a << " > " << ixn.b; break;
                    case gtir: tsm << ixn.a << " > r" << ixn.b; break;
                    case gtrr: tsm << "r" << ixn.a << " > r" << ixn.b; break;
                    case eqri: tsm << "r" << ixn.a << " == " << ixn.b; break;
                    case eqir: tsm << ixn.a << " == r" << ixn.b; break;
                    case eqrr: tsm << "r" << ixn.a << " == r" << ixn.b; break;

                    case addi: tsm << "r" << ixn.a << " + " << ixn.b; break;
                    case muli: tsm << "r" << ixn.a << " * " << ixn.b; break;
                    case bani: tsm << "r" << ixn.a << " & " << ixn.b; break;
                    case bori: tsm << "r" << ixn.a << " | " << ixn.b; break;

                    case addr: tsm << "r" << ixn.a << " + r" << ixn.b; break;
                    case mulr: tsm << "r" << ixn.a << " * r" << ixn.b; break;
                    case banr: tsm << "r" << ixn.a << " & r" << ixn.b; break;
                    case borr: tsm << "r" << ixn.a << " | r" << ixn.b; break;

                    default:
                        cout << "What is " << ixn.op_name << endl;
                }
                tsm << endl;
            }

            ip++;
            last = ixn;
        }
    }
}

void cpu::run(program const& program) {
    while(true) {
        auto ip = r[program.ip_register];
        if (ip < 0 || ip >= program.instructions.size())
            break;
        auto& ixn = program.instructions[ip];
        execute(ixn, program.ip_register);
    }
}
void cpu::execute(const instruction& ixn, int ip_register) {
    auto ip = r[ip_register];
    auto op = ixn.op;
    auto a = ixn.a, b = ixn.b, c = ixn.c;

    if (debug) {
        cout << "ip=" << ip << " [";
        copy(r, r+6, ostream_iterator<int>(cout, ", "));
        cout << "] " << ixn.op_name << " " << a << " " << b << " " << c << " [";
    }

    switch(op) {
        case addr: r[c] = r[a] + r[b]; break;
        case addi: r[c] = r[a] + b; break;
        case mulr: r[c] = r[a] * r[b]; break;
        case muli: r[c] = r[a] * b; break;
        case banr: r[c] = r[a] & r[b]; break;
        case bani: r[c] = r[a] & b; break;
        case borr: r[c] = r[a] | r[b]; break;
        case bori: r[c] = r[a] | b; break;
        case setr: r[c] = r[a]; break;
        case seti: r[c] = a; break;
        case gtir: r[c] = a > r[b] ? 1 : 0; break;
        case gtri: r[c] = r[a] > b ? 1 : 0; break;
        case gtrr: r[c] = r[a] > r[b] ? 1 : 0; break;
        case eqir: r[c] = a == r[b] ? 1 : 0; break;
        case eqri: r[c] = r[a] == b ? 1 : 0; break;
        case eqrr: r[c] = r[a] == r[b] ? 1 : 0; break;
        default: throw std::invalid_argument("Unknown opcode");
    }

    if (debug) {
        copy(r, r+6, ostream_iterator<int>(cout, ", "));
        cout << "]" << endl;
    }

    r[ip_register]++;
}

void cpu::show_registers() {
    cout << "Registers: ";
    copy(r, r+6, ostream_iterator<int>(cout, " "));
    cout << endl;
}

void cpu::set_register(int reg, int value) {
    r[reg] = value;
}
